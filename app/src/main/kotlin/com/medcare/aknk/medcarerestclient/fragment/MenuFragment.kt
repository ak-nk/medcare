package com.medcare.aknk.medcarerestclient

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.medcare.aknk.medcarerestclient.model.ChatManager

class MenuFragment : Fragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val menu = arguments?.getString("mainAction", "default") ?: 0
    when(menu) {
        "home" -> {
            val intent: Intent = Intent(this.context, RegistActivity::class.java)
            startActivity(intent, null)
            return inflater.inflate(R.layout.activity_main, container, false)
        }
        "login" -> {
            val intent: Intent = Intent(this.context, SignInActivity::class.java)
            startActivity(intent, null)
            return inflater.inflate(R.layout.login_fragment, container,false)
        }
        "data" -> {
            return inflater.inflate(R.layout.sample_fragment_3, container, false)
        }
        "chat" -> {
            val intent: Intent = Intent(this.context, ChatActivity::class.java)
            if(intent.extras == null) {
                intent.putExtra("pls_login", "ログインしてください。")
                return inflater.inflate(R.layout.login_fragment, container, false)
            } else {

              startActivity(intent, null)
              return inflater.inflate(R.layout.chat_recycler_view, container, false)
            }
        }
        else -> {
            return inflater.inflate(R.layout.activity_main, container,false)
        }

    }
  }
}
