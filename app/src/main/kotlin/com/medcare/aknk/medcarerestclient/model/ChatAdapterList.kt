package com.medcare.aknk.medcarerestclient.model

import android.app.Application
import android.util.Log
import android.view.*
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.widget.TooltipCompat
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.AndroidViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.medcare.aknk.medcarerestclient.R
import com.medcare.aknk.medcarerestclient.databinding.MyChatTextViewBinding
import kotlinx.coroutines.MainScope

// TODO: ChatMessage is expression for chat ChatMessage

class ChatAdapterList: ListAdapter<ChatMessage, ChatAdapterList.ViewHolder>(ChatMessageDiffCallBack()) {


  override fun onBindViewHolder(holder: ViewHolder, positionId: Int) {
    //To change body of created functions use File | Settings | File Templates.
    val item = getItem(positionId)
    holder.apply {
        // TODO: refactor message id paramter
        bind(item)
        itemView.tag = item
        Log.d("", "${itemView.isLongClickable}")
    }
    holder.itemView.setOnLongClickListener {
      Log.d("", "${it.isLongClickable}")

      // TODO
      return@setOnLongClickListener true
    }
  }


    override fun onCreateViewHolder(group: ViewGroup, position: Int): ViewHolder {
      //To change body of created functions use File | Settings | File Templates
      return ViewHolder(
        DataBindingUtil.inflate(
          LayoutInflater.from(group.context),
          R.layout.my_chat_text_view, null, false
        )
      )
    }


    class ViewHolder(
        private val binding: MyChatTextViewBinding
      ) : RecyclerView.ViewHolder(binding.root) {
      fun bind(item: ChatMessage) {
          binding.apply {
            // TODO: 投稿者の設定
            userName.setText(item.comment_type)
            userChatCard.setText(item.content)
          }
        }
      }

}

private class ChatMessageDiffCallBack: DiffUtil.ItemCallback<ChatMessage>() {

  override fun areItemsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
    // TODO: messageのidを渡す必要がある
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
    return oldItem == newItem
  }

}
