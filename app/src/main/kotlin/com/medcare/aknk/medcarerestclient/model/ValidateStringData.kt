package com.medcare.aknk.medcarerestclient.model

import android.util.Log

class ValidateStringData {
  //　使われていない電話番号のバリデーション
  fun validateStringType(inputed_string: Any): String {
    val validateString = inputed_string.toString()
    val phoneNumberRegex = """^\d{3}-\d{3,}-\d{4,}$""".toRegex()

    if (validateString.isEmpty()) {
      return "this text is empty. pls input some text."
    } else if (validateString.matches(phoneNumberRegex)) {
      Log.d("", "整形後: " + validateString.replace("-", ""))
      return "this text is phone number"
    } else {
      return "-(ハイフン)で区切った電話番号を入力してください(最低10文字)"
    }
  }

}