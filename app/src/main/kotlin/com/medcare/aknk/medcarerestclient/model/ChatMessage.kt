package com.medcare.aknk.medcarerestclient.model

import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import com.github.kittinunf.fuel.Fuel
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.ParameterizedType
import java.util.*

data class ChatMessage(
  val id: Int?,
  val user_id: Int?,
  val lite_user_id: String?,
  val dietician_avatar_id: Int?,
  val content: String?,
  val created_at: String?,
  val comment_type: String?,
  val type: String?,
  val writer: String?
) {}

class ChatManager() {

  fun getChatMessage(moshiAdapter: JsonAdapter<ChatMessageCommentType>, moshiComment: ChatMessageCommentType, handler: Handler, messageList: ArrayList<ChatMessage>, rAdapter: ChatAdapterList) {
    var messagesJson: JSONArray

    //ここでやってるのはdieticianに連なるコメントの取得だけ
    Fuel.get("/users/me/comments")
      .body(moshiAdapter.toJson(moshiComment)).response {
          request, response, result ->
        messagesJson = JSONArray(String(response.data))

        Log.d("", "messageJson: ${messagesJson}")

        when(response.responseMessage) {
          "OK" -> {
            try {
              val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
              val castType: ParameterizedType = Types.newParameterizedType(List::class.java, ChatMessage::class.java)
              val adapter: JsonAdapter<List<ChatMessage>> = moshi.adapter<List<ChatMessage>>(castType)
              //このリストがOnCreate時に保持されるようにしたい
              val parseToList: List<ChatMessage>? = adapter.fromJson(String(response.data))

              if(parseToList != null) {
                messageList.addAll(parseToList)
                Collections.reverse(parseToList)
              }

              handler.post {
                rAdapter.submitList(parseToList)
              }

            } catch(ex : Exception) {
              Log.w("", "exception occured pending cast messages: ${ex}")
            }
          }
          else -> {
            Log.w("", "dump stack : ${Thread.dumpStack()}")
          }
        }

      }
  }



  fun createChatMessage(handler: Handler, message: String, moshiMessageAdapter: JsonAdapter<ChatMessage>,
                        adapter: ChatAdapterList, messageList: ArrayList<ChatMessage>) {

    var moshiMessage = ChatMessage(null,null, "",null, message, "", "dietician",
      "", "")


    Fuel.post("/users/me/text_comments")
      .body(moshiMessageAdapter.toJson(moshiMessage)).response {
          request, response, result ->
        Log.d("", "chat response message?: ${response.responseMessage}")
        Log.d("", "chat response?: ${response}")


        val body = String(response.data)

        Log.d("", "string body: ${body}")

        val message_id = JSONObject(body).get("id").toString()

        when(response.responseMessage) {
          "Created" -> {
            messageList.add(ChatMessage(message_id.toInt(), null, "", null, message, "", "dietician", "", ""))
            messageList.reverse()
            val castMessageList: List<ChatMessage> = messageList.toList()
            handler.post {
              adapter.submitList(castMessageList)
            }

          }
          "Bad Request" -> {
            Log.d("", "failure : ${result}")

          }
          else -> {
            Log.d("", "chat message response :${response}")
          }
        }
      }
  }

}


data class ChatMessageCommentType(
  val commentType: String?
): LiveData<String>() {

}

