package com.medcare.aknk.medcarerestclient.model

import com.squareup.moshi.JsonReader
import kotlin.coroutines.suspendCoroutine

/* *
*
*
* */
class ParamsUtil {
  // 4桁のコードを発行する
  fun generateRandomCode(): String {
    val randomArray = (0..9).shuffled().listIterator(6)
    val buffer = StringBuilder()
    randomArray.forEach { buffer.append(it) }
    return buffer.toString()
  }
  
  suspend fun preparePost(): JsonReader.Token {
    return suspendCoroutine { }
  }
  
  
}