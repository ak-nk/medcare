package com.medcare.aknk.medcarerestclient.model

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.snackbar.Snackbar
import com.medcare.aknk.medcarerestclient.RegistActivity
import com.medcare.aknk.medcarerestclient.RegistUserProfileActivity
import java.util.*


data class User(var phone_number: String?,
                var access_key: String?) {

  companion object {
    fun profileRegistEntry(requestPath: String,
                           accessKey: String,
                           parseMoshi: String,
                           it: View,
                           self: Context) {

      Fuel.post("${requestPath}/${accessKey}/confirm").body(parseMoshi).response { req, res, result ->
        Log.d("", "confirm！ : ${res}")
        Log.d("", "moshi : ${parseMoshi}")

        val snackbar = Snackbar.make(it, "電話番号の登録に成功しました。", Snackbar.LENGTH_LONG)

        when(res.statusCode) {
          200 -> {
            snackbar
              .setActionTextColor(Color.WHITE)
              .setAction("新規登録する") {

                val dispatchIntent: Intent = Intent(self, RegistUserProfileActivity::class.java)
                dispatchIntent.putExtra(RegistActivity.ACCESS_KEY, accessKey)
                startActivity(self, dispatchIntent, null)
              }
              .show()
          }
          else -> {
            Log.d("", "認証キーの確認 : " + result)

            snackbar.setActionTextColor(Color.RED)
            snackbar.setText("電話番号の設定に失敗しました。もう一度お試しください。")
            snackbar.show()
          }
        }
      }
    }

  }
}

data class UserDetail(
  var first_name: String,
  var last_name: String,
  var birthday: String,
  var device_type: String,
  var email: String,
  var password: String,
  var access_key: String
) {}



data class GetConfirmCodeParameter (
  var phone_number: String
) {
}

data class ConfirmCodeParameter (
  var code: String
) {
}

data class SignInUserData (
  var name: String?,
  var email: String?,
  var password: String?,
  var headers: Map<String, List<String>>?
): Observer {
  override fun update(o: Observable?, arg: Any?) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}

data class StoreSession(
  var accessToken: String?,
  var client: String?,
  var expiry: String?,
  var uid: String?
) {

}

class LoggingUtil() {
  fun createLoggingTime():Int {
    val calendar: Calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
    val hour: Int = calendar.get(Calendar.HOUR)
    val min: Int = calendar.get(Calendar.MINUTE)
    val millisec: Int = calendar.get(Calendar.MILLISECOND)

    return Log.d("", "created date ->${hour}時${min}分${millisec}秒")

  }

}