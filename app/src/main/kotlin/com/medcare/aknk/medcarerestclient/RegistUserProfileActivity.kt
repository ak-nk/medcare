package com.medcare.aknk.medcarerestclient

import android.content.Intent
import android.content.pm.SigningInfo
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.medcare.aknk.medcarerestclient.model.*
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class RegistUserProfileActivity : AppCompatActivity() {

  @BindView(R.id.user_device_type) lateinit var device_type: Spinner
  @BindView(R.id.user_email) lateinit var email: EditText
  @BindView(R.id.user_first_name) lateinit var first_name: TextInputEditText
  @BindView(R.id.user_last_name) lateinit var last_name: TextInputEditText
  @BindView(R.id.user_last_name) lateinit var birthday: TextInputEditText
  @BindView(R.id.user_device_type) lateinit var password: TextInputEditText
  @BindView(R.id.prof_regist_btn) lateinit var registBtn: Button


  lateinit var spinner_adapter: ArrayAdapter<CharSequence>

  private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()


  val signIn = SignIn()
  val bundle: Bundle = Bundle()
  var userDetailProfile: SignInUserData = SignInUserData(null, null, null, null)
  var userSessionData: StoreSession = StoreSession(null, null, null, null)
  lateinit var userDetail: UserDetail


  private lateinit var accessKey: String
  private val detailParseMoshi = moshi.adapter(UserDetail::class.java)
  private lateinit var userJson: String

  private lateinit var userDetailAdapter: JsonAdapter<UserDetail>
  private lateinit var dispatchChatIntent: Intent

  private lateinit var mModel: SignIn

  val success: (ByteArray) -> String = {
    "success"
  }
  val failure: (FuelError) -> String = {
    "failure"
  }


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.input_profile)

    accessKey = getIntent().getStringExtra(RegistActivity.ACCESS_KEY)

    Log.d("", "dispatch params : ${accessKey.toString()}")


    device_type = findViewById(R.id.user_device_type)

    spinner_adapter = ArrayAdapter.createFromResource(this, R.array.device_type, android.R.layout.simple_spinner_dropdown_item)
    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    device_type.setAdapter(spinner_adapter)
    device_type.onItemSelectedListener

    val createUserInfo: () -> Pair<JsonAdapter<UserDetail>, UserDetail?> = {
      val gson = Gson()
      email = findViewById(R.id.user_email)
      first_name = findViewById(R.id.user_first_name)
      last_name = findViewById(R.id.user_last_name)
      birthday = findViewById(R.id.user_birthday)
      // ヘイブンなのが気になる
      password = findViewById(R.id.user_password)

      userDetail = UserDetail(
        first_name.text.toString(),
        last_name.text.toString(),
        birthday.text.toString(),
        device_type.selectedItem.toString(),
        email.text.toString(),
        password.text.toString(),
        accessKey
      )

      userJson = gson.toJson(userDetail)
      var userData = detailParseMoshi.fromJson(userJson)

      //  サーバへポストする用にJsonのアダプタMoshiをユーザープロフィールとセットで返却する
      Pair(detailParseMoshi, userData)
    }

    registBtn = findViewById(R.id.prof_regist_btn)

    registBtn.setOnClickListener {
      userDetailAdapter = createUserInfo().first
      val userDetail = createUserInfo().second
      mModel = ViewModelProviders.of(this).get(SignIn::class.java)

      mModel.mutableSinginData.observe(this, Observer { signUserData ->
        Log.d("", "data: ${signUserData}")

        signUserData.name = signUserData.name.toString()
        signUserData.email = signUserData.email.toString()
        signUserData.password = signUserData.password.toString()
        signUserData.headers = signUserData.headers
        signUserData?.let {
          // intent update
          dispatchChatIntent = Intent(this, ChatActivity::class.java)
          bundle.putString("email", signUserData.email.toString())
          bundle.putString("password", signUserData.password.toString())
          bundle.putString("headers", signUserData.headers.toString())
          bundle.putString("user_name", signUserData.name.toString())
          dispatchChatIntent.putExtras(bundle)
          Log.d("", "data: ${signUserData}")
        }
      })

      mModel.mutableSessionData.observe(this, Observer { sessionData ->
        Log.d("", "session: ${sessionData}")

        sessionData.accessToken = sessionData.accessToken
        sessionData.expiry = sessionData.expiry
        sessionData.uid = sessionData.uid
        sessionData.client = sessionData.client

        sessionData?.let {
          bundle.putString("access-token", sessionData.accessToken)
          bundle.putString("client", sessionData.client)
          bundle.putString("expiry", sessionData.expiry)
          bundle.putString("uid", sessionData.uid)
          dispatchChatIntent.putExtras(bundle)
        }
      })


      runBlocking{
        Fuel.post("/auth").body(userDetailAdapter.toJson(userDetail))
          .response { req, res, result ->
            // TODO : バリデーションなど
            val snackbar = Snackbar.make(it,
              "登録に失敗しました。通信状況の良いところでもう一度お試しいただくか、管理者までご連絡ください。",
              Snackbar.LENGTH_LONG)

            var resultText = ""
            if(resultText == null) {
              resultText = result.fold(success, failure)
            }

            when(resultText) {
              "success" -> {
                userDetailProfile.name = userDetail?.first_name
                userDetailProfile.email = userDetail?.email
                userDetailProfile.password = userDetail?.password
                userDetailProfile.headers = res.headers.toMap()
                userSessionData.accessToken = res.headers["access-token"].toString().trim('[', ']')
                userSessionData.client = res.headers["client"].toString().trim('[', ']')
                userSessionData.expiry = res.headers["expiry"].toString().trim('[', ']')
                userSessionData.uid = res.headers["uid"].toString().trim('[', ']')

                startActivity(dispatchChatIntent)

              }
              "failure" -> {
                snackbar.show()
                Log.d("", "Request was failed:${res.responseMessage}")
                Log.d("", "${res}")
              }
              else -> {
                Thread.dumpStack()
                snackbar.show()
              }

            }

          }
      }


    }
  }
}