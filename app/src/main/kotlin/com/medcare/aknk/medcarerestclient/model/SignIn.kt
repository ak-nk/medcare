package com.medcare.aknk.medcarerestclient.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class SignIn : ViewModel(), Observer<String> {

  override fun onChanged(t: String?) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    Log.d("", "t: ${t}")
  }
  val userSignInData = SignInUserData(null, null, null, null)
  val userSessionData = StoreSession(null, null, null, null)
  var mutableSinginData: MutableLiveData<SignInUserData> = MutableLiveData()
  var mutableSessionData: MutableLiveData<StoreSession> = MutableLiveData()
  val success: (ByteArray) -> String = {
    "success"
  }
  val failure: (FuelError) -> String = {
    "failure"
  }


  fun signIn(userDetail: SignInUserData?, sessionData: StoreSession?):
    LiveData<Pair<SignInUserData, StoreSession>> {

    val resultLiveData = MutableLiveData<Pair<SignInUserData, StoreSession>>()
      // 1
      Log.d("", "User data2 : ${userDetail.toString()}")

      val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
      val detailParseMoshi = moshi.adapter(SignInUserData::class.java)
      val flag = true

    FuelManager.instance.baseHeaders = mapOf("Content-Type" to "application/json")
      // 2
      Fuel.post("/auth/sign_in").body(detailParseMoshi.toJson(userDetail)).response {
          req, res, result ->
          Log.d("", "Res:${res}")
          Log.d("", "Res:${res.headers}")
        val resultText = result.fold(success, failure)
        when(resultText) {
          "success" -> {
            // Unauthorized
            userSignInData.name = userDetail?.name.toString()
            userSignInData.email = userDetail?.email.toString()
            userSignInData.password = userDetail?.password.toString()
            userSignInData.headers = res?.headers
            userSessionData.uid = res.headers["uid"].toString().trim('[', ']')
            userSessionData.expiry = res.headers["expiry"].toString().trim('[', ']')
            userSessionData.client =  res.headers["client"].toString().trim('[', ']')
            userSessionData.accessToken = res.headers["access-token"].toString().trim('[', ']')
            mutableSinginData.postValue(userSignInData)
            mutableSessionData.postValue(userSessionData)
            resultLiveData.postValue(Pair(userSignInData, userSessionData))
          }
          "failure" -> {
            if(res.responseMessage == "Unauthorized") {
              Log.d("", "Unmatch data, pls check your mailaddress or password : ${result}")
            }; Log.d("", "Fail :User data : ${result}")
            Log.d("", "Fail :User singin data : ${userSignInData}")
            Log.d("", "Fail :User session data : ${userSessionData}")
            return@response
          }
        }
      }

    return resultLiveData
//    return Pair(userSignInData, userSessionData)


  }
  
}