//package com.medcare.aknk.medcarerestclient.model
//
//import androidx.fragment.app.FragmentActivity
//import android.R
//import androidx.fragment.app.FragmentTabHost
//import android.os.Bundle
//
//
//class FragmentTabs: FragmentActivity() {
//  private var mTabHost: FragmentTabHost? = null
//
//  override fun onCreate(savedInstanceState: Bundle?) {
//    super.onCreate(savedInstanceState)
//
//    setContentView(R.layout.sample_fragment)
//    mTabHost = findViewById<View>(android.R.id.tabhost)
//    mTabHost!!.setup(this, supportFragmentManager, R.id.realtabcontent)
//
//    mTabHost!!.addTab(
//      mTabHost!!.newTabSpec("simple").setIndicator("Simple"),
//      FragmentStackSuppåort.CountingFragment::class.java!!, null
//    )
//    mTabHost!!.addTab(
//      mTabHost!!.newTabSpec("contacts").setIndicator("Contacts"),
//      LoaderCursorSupport.CursorLoaderListFragment::class.java!!, null
//    )
//    mTabHost!!.addTab(
//      mTabHost!!.newTabSpec("custom").setIndicator("Custom"),
//      LoaderCustomSupport.AppListFragment::class.java!!, null
//    )
//    mTabHost!!.addTab(
//      mTabHost!!.newTabSpec("throttle").setIndicator("Throttle"),
//      LoaderThrottleSupport.ThrottledLoaderListFragment::class.java!!, null
//    )
//  }
//}