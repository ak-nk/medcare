package com.medcare.aknk.medcarerestclient

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.medcare.aknk.medcarerestclient.model.SignIn
import com.medcare.aknk.medcarerestclient.model.SignInUserData
import com.medcare.aknk.medcarerestclient.model.StoreSession
import com.medcare.aknk.medcarerestclient.model.UserDetail
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.runBlocking

class SignInActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, Observer<String> {

  @BindView(R.id.user_email) lateinit var email: EditText
  @BindView(R.id.user_device_type) lateinit var password: TextInputEditText
  @BindView(R.id.prof_regist_btn) lateinit var loginBtn: Button

  val signIn = SignIn()
  val bundle: Bundle = Bundle()
  var userDetailProfile: SignInUserData = SignInUserData(null, null, null, null)
  var userSessionData: StoreSession = StoreSession(null, null, null, null)
  lateinit var userDetail: UserDetail

  private lateinit var dispatchChatIntent: Intent


  override fun onChanged(t: String?) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

    Log.d("", "observe now : ${t}")
  }

  override fun onNothingSelected(parent: AdapterView<*>?) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  private lateinit var mModel: SignIn

  val success: (ByteArray) -> String = {
    "success"
  }
  val failure: (FuelError) -> String = {
    "failure"
  }


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.login_fragment)
    

    loginBtn = findViewById(R.id.prof_regist_btn)

    loginBtn.setOnClickListener {
      mModel = ViewModelProviders.of(this).get(SignIn::class.java)
      dispatchChatIntent = Intent(this, ChatActivity::class.java)

      email = findViewById(R.id.user_login_email)
      password = findViewById(R.id.user_login_password)


      userDetail = UserDetail(
        "", "", "", "",
        email.text.toString(),
        password.text.toString(),
        ""
      )

      // TODO : バリデーションなど
      val snackbar = Snackbar.make(it,
        "登録に失敗しました。通信状況の良いところでもう一度お試しいただくか、管理者までご連絡ください。",
        Snackbar.LENGTH_LONG)

      FuelManager.instance.basePath = "https://api.beta.medically.co.jp:443"

      userDetailProfile.email = userDetail?.email
      userDetailProfile.password = userDetail?.password

      val userData = signIn.signIn(userDetailProfile, userSessionData)
      userData.observe(this,Observer {
        mModel.mutableSinginData.postValue(it.first)
        mModel.mutableSessionData.postValue(it.second)

        bundle.putString("email", it.first.email.toString())
        bundle.putString("password", it.first.password.toString())
        bundle.putString("user_name", it.first.name.toString())

        bundle.putString("access-token", it.second.accessToken)
        bundle.putString("client", it.second.client)
        bundle.putString("expiry", it.second.expiry)


        dispatchChatIntent.putExtras(bundle)

        startActivity(dispatchChatIntent)
      })

    }
  }
}