package com.medcare.aknk.medcarerestclient

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import butterknife.BindView
import com.github.kittinunf.fuel.core.FuelManager
import com.medcare.aknk.medcarerestclient.model.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class ChatActivity : AppCompatActivity() {

  @BindView(R.id.chat_recycler_view) lateinit var recyclerView: RecyclerView
  @BindView(R.id.chat_message_input) lateinit var chatMessage: TextView
  @BindView(R.id.user_first_name) lateinit var sendButton: Button
  @BindView(R.id.user_last_name) lateinit var toolBar: Toolbar
  @BindView(R.id.parent_card_view) lateinit var chatCardTextFocus: RecyclerView.ViewHolder

  private lateinit var viewAdapter: RecyclerView.Adapter<ChatAdapterList.ViewHolder>
  private lateinit var viewManager: LinearLayoutManager

  private lateinit var userChatCard: TextView
  private lateinit var userName: TextView

  private var message: String = ""

  private val moshi: Moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()
  private val moshiMessageAdapter = moshi.adapter(ChatMessage::class.java)
  private val moshiCommentAdapter = moshi.adapter(ChatMessageCommentType::class.java)
  private val moshiSessionAdapter = moshi.adapter(StoreSession::class.java)
  private lateinit var moshiMessage: ChatMessage
  private lateinit var moshiCommentType: ChatMessageCommentType
  private lateinit var moshiStoreSession: StoreSession


  private val chatManager = ChatManager()

  private val adapter: ChatAdapterList = ChatAdapterList()
  private var messageList = arrayListOf<ChatMessage>()
//  private var messageList: List<ChatMessage>? = listOf()
  lateinit var uid: String
  lateinit var accessToken: String
  lateinit var client: String
  lateinit var expiry: String

  private val handler: Handler = Handler()


  // 謎い
  private val observer: RecyclerView.AdapterDataObserver = object : RecyclerView.AdapterDataObserver() {
    override fun onChanged() {
      super.onChanged()
      recyclerView.scrollToPosition(0)
    }

    override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
      super.onItemRangeChanged(positionStart, itemCount)
      recyclerView.scrollToPosition(0)

    }

    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
      super.onItemRangeInserted(positionStart, itemCount)
      recyclerView.scrollToPosition(0)

    }

    override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
      super.onItemRangeMoved(fromPosition, toPosition, itemCount)
      recyclerView.scrollToPosition(0)

    }

    override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
      super.onItemRangeRemoved(positionStart, itemCount)
      recyclerView.scrollToPosition(0)

    }
  }



  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.chat_recycler_view)

    adapter.registerAdapterDataObserver(observer)

    // TODO: commtent_typeの変更
    moshiCommentType = ChatMessageCommentType("dietician")

    userName = findViewById(R.id.user_name_text)
    val userNameText = intent.extras.getString("user_name")
    userName.let {
      if(it == null) {
        it.setText(userNameText)
      }
    }

    recyclerView = findViewById(R.id.chat_recycler_view)

    viewManager = LinearLayoutManager(this, VERTICAL, true)

    chatMessage = findViewById(R.id.chat_message_input)

    sendButton = findViewById(R.id.chat_send_button)
    toolBar = findViewById(R.id.chat_toolbar)

    recyclerView.layoutManager = viewManager
    recyclerView.adapter = adapter


    try {
        FuelManager.instance.basePath = "https://api.beta.medically.co.jp:443"
        FuelManager.instance.baseHeaders = mapOf(
        "access-token" to intent.extras.getString("access-token"),
        "uid" to intent.extras.getString("uid"),
        "client" to intent.extras.getString("client"),
        "expiry" to intent.extras.getString("expiry"),
        "Content-Type" to "application/json"
        )

    } catch(ex: Exception) {
      Log.d("", "${ex}")
    }

    sendButton.setOnClickListener {

      message = chatMessage.text.toString()

      // TODO: content validation for blank content error
      chatManager.createChatMessage(handler, message, moshiMessageAdapter, adapter, messageList)

      viewManager.scrollToPositionWithOffset(0, 0)

      chatMessage.text = null

    }


  }

  override fun onStart() {
    super.onStart()
//    TODO:セッションがなければreturn?
    val chatMessage = ChatManager()
    val handler = Handler()

    chatMessage.getChatMessage(moshiCommentAdapter,moshiCommentType, handler, messageList, adapter)

    viewManager.scrollToPositionWithOffset(0, 0)

  }
}