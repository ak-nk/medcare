package com.medcare.aknk.medcarerestclient

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.google.android.material.snackbar.Snackbar
import com.medcare.aknk.medcarerestclient.model.ConfirmCodeParameter
import com.medcare.aknk.medcarerestclient.model.GetConfirmCodeParameter
import com.medcare.aknk.medcarerestclient.model.ParamsUtil
import com.medcare.aknk.medcarerestclient.model.User
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.json.JSONObject

class RegistActivity : AppCompatActivity() {

  @BindView(R.id.regist_button) lateinit var registBtn: Button
  @BindView(R.id.user_phone_number) lateinit var inputString: EditText


  // 乱数作成のクラスを呼び出す
  private val digitNum = ParamsUtil()

  private val phoneNumParamsMap: MutableMap<String, String> = mutableMapOf()
  lateinit var validatedPhoneNum: String
  private val moshi: Moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()
  private val moshiParseConfirmRegistAdaptaer = moshi.adapter(GetConfirmCodeParameter::class.java)
  private val moshiParseConfirmCodeAdaptaer = moshi.adapter(ConfirmCodeParameter::class.java)

  private lateinit var moshiPhoneNum: GetConfirmCodeParameter
  private lateinit var confirmCode: ConfirmCodeParameter

  private lateinit var responseJson: String
  private lateinit var accessKey: String
  private lateinit var resultText: String

  val success: (ByteArray) -> String = {
    "success"
  }
  val failure: (FuelError) -> String = {
    "failure"
  }

  companion object {
    val ACCESS_KEY = "com.medcare.aknk.medcarerestclient"
  }
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val fragmentManager = supportFragmentManager
    val fragmentTran = fragmentManager.beginTransaction()

    val fragment = SampleFragment()

    fragmentTran.add(R.id.activity_main, fragment)
    fragmentTran.commit()

    // listener set
    registBtn = findViewById(R.id.regist_button)
    inputString = findViewById(R.id.user_phone_number)

    // ベースURL,ヘッダーを設定
    FuelManager.instance.basePath = "https://api.beta.medically.co.jp:443"
    FuelManager.instance.baseHeaders = mapOf("Content-Type" to "application/json")

    registBtn.setOnClickListener {

      validatedPhoneNum = inputString.text.toString()

      phoneNumParamsMap.put("phone_number", validatedPhoneNum)

      // SMS認証のパート
      // 招待コードを投げる
      // TODO: リファクタ
      val codeParamsMap: MutableMap<String, String> = mutableMapOf()
      codeParamsMap.put("code", digitNum.generateRandomCode())

      moshiPhoneNum = GetConfirmCodeParameter(validatedPhoneNum)
      confirmCode = ConfirmCodeParameter(digitNum.generateRandomCode())

      Log.d("", "how json view? > : ${moshiParseConfirmRegistAdaptaer.toJson(moshiPhoneNum)}")

      val moshiNum = moshiParseConfirmRegistAdaptaer.toJson(moshiPhoneNum)
      val moshiCode = moshiParseConfirmCodeAdaptaer.toJson(confirmCode)
      Log.d("", "moshi number :${moshiNum}")

      val sms_path = "sms_authentications"


      Fuel.post(sms_path).body(moshiParseConfirmRegistAdaptaer.toJson(moshiPhoneNum))
        .response { request, response, result ->

          resultText = result.fold(success, failure)
          Log.d("", "レスポンス : " + String(response.data))
          Log.d("", "リクエスト : " + request)
          Log.d("", "結果 : " + resultText)

          when(resultText) {
            "success" -> {
              // access_keyを文字列に変換してパスにする
              responseJson = String(response.data)
              accessKey = JSONObject(responseJson).get("access_key").toString()

              Log.d("", "access key : ${accessKey}")

              Fuel.get("${sms_path}/${accessKey}").response { _, res, result ->
                Log.d("", "認証キーの確認 : " + result)

                val snackbar = Snackbar.make(it, "認証の確認に成功しました。", Snackbar.LENGTH_INDEFINITE)

                Log.d("", "get response :${res.responseMessage}")

                resultText = result.fold(success, failure)

                // TODO: メッセージ表示のdelayが必要
                when (resultText) {
                  "success" -> {
                    Log.d(
                    "",
                    "sms_path: ${sms_path}\n" +
                      "accessKey: ${accessKey}\n" +
                      "View: ${it}\n" +
                      "self: ${this@RegistActivity}\n"
                    )

                    snackbar.show()
                    User.profileRegistEntry(sms_path,  accessKey, moshiCode, it, this)
                  }
                  "failure" -> {
                    snackbar.setActionTextColor(Color.RED)
                    snackbar.setText("認証に失敗しました。もう一度お試しいただくか、管理者にお問い合わせください。")
                    snackbar.show()
                  }
                  else -> {
                  }
                }
              }
            }
            "failure" -> {
              Log.d("", "認証の確認に失敗しました。: ${result}")
            }
            else -> {}
          }

        }

    }
  }

}
